package it.uniupo.algotools;

import it.uniupo.graphlib.UndirectedGraph;

/**
 * An interface to handle Minimum Spanning Tree (MST).
 *
 * @author Lavinia Egidi
 */
public interface MST {
    MST create(UndirectedGraph graph);

    UndirectedGraph getMST();

    int getCost();
}
