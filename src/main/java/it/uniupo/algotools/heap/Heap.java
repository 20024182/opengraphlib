package it.uniupo.algotools.heap;

/**
 * An interface for common methods of a priority queue.
 *
 * @param <E> type of the item
 * @param <K> type of the key with respect to which the order is maintained
 * @author Lavinia Egidi
 */
public interface Heap<E, K extends Comparable<K>> {

    /**
     * Whether the heap is empty.
     *
     * @return {@code true} if the heap is empty, {@code false} otherwise
     */
    boolean isEmpty();

    /**
     * Adds an element to the heap.
     *
     * @param e the item to be added
     * @param c the key with respect to which the order is maintained
     */
    void add(E e, K c);

    /**
     * Checks if heap contains element {@code e}.
     *
     * @param e the item searched
     * @return {@code true} if it is contained, {@code false} otherwise
     */
    boolean contains(E e);

    /**
     * @return the number of elements in the heap
     */
    int getSize();
}
