package it.uniupo.algotools.heap;

/**
 * A concrete implementation of max priority queue.
 *
 * @author Lavinia Egidi
 * @author Lorenzo Ferron
 * @see it.uniupo.algotools.heap.Heap
 */
public class MaxHeap<E, K extends Comparable<K>> extends AbstractHeap<E, K> {
    @Override
    protected void bubble(int i) {
        if (i > 0 && this.heap.get(i).getCost().compareTo(this.heap.get(i / 2).getCost()) > 0) {
            MaxHeap<E, K>.WeightedItem tmp = this.heap.get(i);
            this.heap.set(i, this.heap.get(i / 2));
            this.heap.set(i / 2, tmp);
            this.bubble(i / 2);
        }
    }

    @Override
    protected void sink(int i) {
        int heapTop = this.getSize();
        if (2 * i < heapTop && this.heap.get(i).getCost().compareTo(this.heap.get(2 * i).getCost()) < 0 || 2 * i + 1 < heapTop && this.heap.get(i).getCost().compareTo(this.heap.get(2 * i + 1).getCost()) < 0) {
            MaxHeap<E, K>.WeightedItem tmp = this.heap.get(i);
            if (2 * i + 1 < heapTop && this.heap.get(2 * i).getCost().compareTo(this.heap.get(2 * i + 1).getCost()) < 0) {
                this.heap.set(i, this.heap.get(2 * i + 1));
                this.heap.set(2 * i + 1, tmp);
                this.sink(2 * i + 1);
            } else {
                this.heap.set(i, this.heap.get(2 * i));
                this.heap.set(2 * i, tmp);
                this.sink(2 * i);
            }
        }
    }

    /**
     * Returns the maximum element in the heap and deletes it.
     *
     * @return the maximum element in the heap
     */
    public E extractMax() {
        return this.extract();
    }

    /**
     * Peeks at the root of the heap (not removing it).
     *
     * @return the maximum element
     */
    public E getMax() {
        return this.get();
    }
}
