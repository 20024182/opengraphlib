package it.uniupo.algotools.heap;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.util.ArrayList;
import java.util.List;

/**
 * An abstract class to implement shared definitions of heap.
 *
 * @author Lorenzo Ferron
 * @see it.uniupo.algotools.heap.Heap
 */
public abstract class AbstractHeap<E, K extends Comparable<K>> implements Heap<E, K> {
    protected List<WeightedItem> heap = new ArrayList<>(1);

    @Override
    public boolean isEmpty() {
        return this.heap.isEmpty();
    }

    @Override
    public void add(E e, K c) {
        this.heap.add(new AbstractHeap<E, K>.WeightedItem(e, c));
        this.bubble(this.getSize() - 1);
    }

    protected abstract void bubble(int i);

    protected E extract() {
        E e = null;
        if (!this.isEmpty()) {
            AbstractHeap<E, K>.WeightedItem ge = this.heap.remove(0);
            if (!this.isEmpty()) {
                this.heap.add(0, this.heap.remove(this.getSize() - 1));
                this.sink(0);
            }
            e = ge.getItem();
        }
        return e;
    }

    protected E get() {
        return this.isEmpty() ? null : this.heap.get(0).getItem();
    }

    protected abstract void sink(int i);

    @Override
    public boolean contains(E e) {
        return this.heap.contains(new AbstractHeap<E, K>.WeightedItem(e, null));
    }

    @Override
    public int getSize() {
        return this.heap.size();
    }

    protected class WeightedItem {
        private final E e;
        private final K c;

        WeightedItem(E item, K cost) {
            this.e = item;
            this.c = cost;
        }

        public E getItem() {
            return this.e;
        }

        public K getCost() {
            return this.c;
        }

        @Override
        public int hashCode() {
            return new HashCodeBuilder(17, 31).
                    append(this.e).
                    toHashCode();
        }

        @Override
        @SuppressWarnings("unchecked")
        public boolean equals(Object obj) {
            // Basic checks.
            if (!(obj instanceof AbstractHeap.WeightedItem)) return false;
            if (obj == this) return true;

            // Property checks.
            AbstractHeap<E, K>.WeightedItem other = (WeightedItem) obj;
            return new EqualsBuilder().
                    append(this.e, other.e).
                    isEquals();
        }
    }
}
