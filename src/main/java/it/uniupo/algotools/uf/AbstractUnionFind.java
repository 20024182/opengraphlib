package it.uniupo.algotools.uf;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * An abstract class to implement shared definitions of union finds.
 *
 * @author Lorenzo Ferron
 */
public abstract class AbstractUnionFind implements UnionFind {
    protected int[] leader;
    protected int numberOfSets;

    /**
     * Creates a new UnionFind object, for {@code num} elements; it initializes it to {@code num}
     * singleton sets.
     *
     * @param num the universe size
     */
    protected AbstractUnionFind(int num) {
        this.leader = new int[num];
        this.numberOfSets = num;

        Arrays.setAll(this.leader, i -> i);
    }

    @Override
    public int getNumberOfSets() {
        return this.numberOfSets;
    }

    @Override
    public String toString() {
        return Arrays.stream(this.leader).mapToObj(j -> j + " ").collect(Collectors.joining()).trim();
    }
}
