package it.uniupo.algotools.uf;

/**
 * An interface to define common operations on union find.
 *
 * @author Lavinia Egidi
 */
public interface UnionFind {

    /**
     * Creates a new object of the same implementation as this, with universe of
     * dimension {@code size}.
     *
     * @param size the size of the universe
     * @return an object of the same class as this
     */
    UnionFind create(int size);

    /**
     * Merges the sets containing {@code a} and {@code b}, if they are different; does nothing if {@code a} and
     * {@code b} belong to the same set.
     *
     * @param a first item
     * @param b second item
     */
    void union(int a, int b);

    /**
     * Returns the name of the leader of the set containing i.
     *
     * @param i the item to be located
     * @return the leader of the set to which {@code i} belongs
     */
    int find(int i);

    /**
     * Returns the number of sets so far.
     *
     * @return the number of distinct sets
     */
    int getNumberOfSets();
}
