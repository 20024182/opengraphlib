package it.uniupo.algotools.uf;

/**
 * A concrete implementation of quick find.
 *
 * @author Lavinia Egidi
 * @author Lorenzo Ferron
 */
public class QuickFind extends AbstractUnionFind {

    /**
     * @param num the universe size
     * @see AbstractUnionFind#AbstractUnionFind(int)
     */
    public QuickFind(int num) {
        super(num);
    }

    @Override
    public UnionFind create(int size) {
        return new QuickFind(size);
    }

    @Override
    public void union(int a, int b) {
        if (this.leader[a] != a || this.leader[b] != b)
            throw new IllegalArgumentException(System.lineSeparator() + "***********************************" + System.lineSeparator() + "ATTENZIONE!!" + System.lineSeparator() + "In questa implementazione il metodo union richiede come" + " input rappresentanti di insiemi!!!" + System.lineSeparator() + "***************************************************");
        if (a != b) {
            for (int i = 0; i < this.leader.length; i++) {
                if (this.leader[i] == b)
                    this.leader[i] = a;
            }
            --this.numberOfSets;
        }
    }

    @Override
    public int find(int i) {
        return this.leader[i];
    }
}
