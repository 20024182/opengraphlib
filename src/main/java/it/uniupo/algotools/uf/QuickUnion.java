package it.uniupo.algotools.uf;

/**
 * A concrete implementation of quick union.
 *
 * @author Lavinia Egidi
 * @author Lorenzo Ferron
 */
public class QuickUnion extends AbstractUnionFind {

    /**
     * @param num the universe size
     * @see AbstractUnionFind#AbstractUnionFind(int)
     */
    protected QuickUnion(int num) {
        super(num);
    }

    @Override
    public UnionFind create(int size) {
        return new QuickUnion(size);
    }

    @Override
    public void union(int a, int b) {
        if (this.leader[a] != a || this.leader[b] != b)
            throw new IllegalArgumentException(System.lineSeparator() + "***********************************" + System.lineSeparator() + "ATTENZIONE!!" + System.lineSeparator() + "In questa implementazione il metodo union richiede come" + " input rappresentanti di insiemi!!!" + System.lineSeparator() + "***************************************************");
        this.leader[b] = a;
        --this.numberOfSets;
    }

    @Override
    public int find(int i) {
        while (this.leader[i] != i)
            i = this.leader[i];
        return i;
    }
}
