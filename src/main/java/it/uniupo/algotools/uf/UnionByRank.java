package it.uniupo.algotools.uf;

/**
 * A concrete implementation of union by rank.
 *
 * @author Lavinia Egidi
 * @author Lorenzo Ferron
 */
public class UnionByRank extends AbstractUnionFind {
    private int[] rank;

    /**
     * Creates a new UnionFind object, for {@code num} elements; it initializes it to {@code num}
     * singleton sets.
     *
     * @param num the universe size
     */
    public UnionByRank(int num) {
        super(num);
        this.rank = new int[this.numberOfSets];
    }

    @Override
    public UnionFind create(int size) {
        return new UnionByRank(size);
    }

    @Override
    public void union(int a, int b) {
        if (this.leader[a] != a || this.leader[b] != b)
            throw new IllegalArgumentException(System.lineSeparator() + "***********************************" + System.lineSeparator() + "ATTENZIONE!!" + System.lineSeparator() + "In questa implementazione il metodo union richiede come" + " input rappresentanti di insiemi!!!" + System.lineSeparator() + "***************************************************");
        if (a != b) {
            if (this.rank[a] < this.rank[b])
                this.leader[a] = b;
            else {
                this.leader[b] = a;
                if (this.rank[a] == this.rank[b])
                    this.rank[a]++;
            }
            --this.numberOfSets;
        }
    }

    @Override
    public int find(int i) {
        while (this.leader[i] != i)
            i = this.leader[i];
        return i;
    }
}
