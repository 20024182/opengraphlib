package it.uniupo.graphlib;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * An abstract class to implement some common methods for directed and undirected
 * graphs.
 *
 * @author Lavinia Egidi
 * @author Lorenzo Ferron
 */
public abstract class AbstractGraph implements GraphInterface {
    protected int order;
    protected int edgeNum;
    protected List<Edge>[] incidenceList;

    /**
     * Instances a graph with the specified number of nodes and no edges.
     *
     * @param order the number of nodes
     */
    @SuppressWarnings("unchecked")
    public AbstractGraph(int order) {
        this.order = order;
        this.incidenceList = new LinkedList[this.order];

        for (int i = 0; i < this.order; i++) this.incidenceList[i] = null;
    }

    /**
     * <p>Instances a graph as described by the input string.</p>
     * <p>
     * Accepted formats:
     * <ul>
     *     <li>"nodes: 5; edges: (3,4) weight 5; (1,2) weight -4; (0,4) weight 2"</li>
     *     <li>"5; 3 4 5; 1 2 -4; 0 4 2"</li>
     *     <li>"5; 3-4 weight 5; 1-2 weight -4; 0-4 weight 2" (no spaces between endpoints and the '-' between them)</li>
     *     <li>"5; 3-4 5; 1-2 -4; 0-4 2" (no spaces between endpoints and the '-' between them)</li>
     *     <li>"5\n 3 4 5\n 1 2 -4\n 0 4 2"</li>
     * </ul>
     * (if weight is omitted, it is set to 0 by default)
     *
     * @param graphDescription description of the graph
     */
    @SuppressWarnings("unchecked")
    public AbstractGraph(String graphDescription) {
        if (graphDescription != null && !graphDescription.isEmpty()) {
            String cleanDescription = graphDescription.replace("\n", ";");
            cleanDescription = cleanDescription.replace(" -", " M").replace("\t-", " M").replace("weight-", " M").replace(":-", " M");
            cleanDescription = cleanDescription.replace('(', ' ').replace(')', ' ').replace(',', ' ').replace(')', ' ');
            cleanDescription = cleanDescription.replace('-', ' ').replace("nodes", "").replace("edges", "");
            cleanDescription = cleanDescription.replace("weight", "").replace(":", "");
            cleanDescription = cleanDescription.replace(" M", " -");
            String[] inputLines = cleanDescription.split(";");
            if (inputLines.length <= 0 || !GraphUtils.isInt(inputLines[0].trim()))
                throw new IllegalArgumentException("The string representing the graph has a wrong format.");
            else {
                this.order = Integer.parseInt(inputLines[0].trim());
                this.incidenceList = new LinkedList[this.order];
                if (this.order <= 0)
                    throw new IllegalArgumentException("The number of nodes must be a positive integer.");
                else {
                    int i;
                    for (i = 0; i < this.order; i++) this.incidenceList[i] = null;

                    int weight, head, tail;
                    for (i = 1; i < inputLines.length; i++) {
                        String[] tokens = inputLines[i].trim().split("\\s+");
                        tail = Integer.parseInt(tokens[0]);
                        head = Integer.parseInt(tokens[1]);
                        weight = tokens.length == 3 ? Integer.parseInt(tokens[2]) : 0;

                        if (head < 0 || head >= this.order || tail < 0 || tail >= this.order)
                            throw new IllegalArgumentException("The string representing the graph has a wrong format or the number of nodes is incompatible with edge endpoint names.");

                        this.addEdge(tail, head, weight);
                    }
                }
            }
        }
    }

    @Override
    public void addEdge(int tail, int head) {
        this.addEdge(tail, head, 0);
    }

    @Override
    public void addEdge(Edge e) {
        this.addEdge(e.getTail(), e.getHead(), e.getWeight());
    }

    @Override
    public boolean hasEdge(int tail, int head) {
        return this.findEdge(tail, head) >= 0;
    }

    /**
     * Returns index of node in incidence list, -1 otherwise.
     *
     * @param tail tail of node
     * @param head head of node
     * @return index of node, -1 otherwise
     */
    protected int findEdge(int tail, int head) {
        return this.incidenceList[tail] == null ? -1 :
                IntStream.range(0, this.incidenceList[tail].size())
                        .filter(i -> this.incidenceList[tail].get(i).getHead() == head)
                        .findFirst()
                        .orElse(-1);
    }

    @Override
    public boolean hasEdge(Edge e) {
        return this.hasEdge(e.getTail(), e.getHead());
    }

    @Override
    public int getOrder() {
        return this.order;
    }

    @Override
    public int getEdgeNum() {
        return this.edgeNum;
    }

    @Override
    public Iterable<Integer> getNeighbors(int u) {
        List<Integer> q = new LinkedList<>();

        if (this.incidenceList[u] != null)
            q = this.incidenceList[u].stream()
                    .map(Edge::getHead)
                    .collect(Collectors.toCollection(LinkedList::new));
        return q;
    }

    @Override
    public Iterable<Edge> getOutEdges(int u) {
        List<Edge> q = new LinkedList<>();

        if (this.incidenceList[u] != null)
            q = this.incidenceList[u].stream()
                    .map(Edge::new)
                    .collect(Collectors.toCollection(LinkedList::new));
        return q;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < this.order; i++) {
            sb.append(i).append(": ");
            for (Edge e : this.getOutEdges(i))
                sb.append(e.getHead()).append(",").append(e.getWeight()).append(" ");
            sb.append("\n");
        }

        return sb.toString();
    }

    @Override
    public GraphInterface create() {
        return this.create(this.getOrder());
    }
}
