package it.uniupo.graphlib;

/**
 * A utility class to manipulate a graph.
 *
 * @author Lavinia Egidi
 * @author Lorenzo Ferron
 */
public class GraphUtils {

    private GraphUtils() {
        // Empty body...
    }

    /**
     * Checks whether a string is a decimal representation of an integer.
     *
     * @param s the string
     * @return {@code true} if the string is integer, {@code false} otherwise
     */
    public static boolean isInt(String s) {
        try {
            Integer.parseInt(s);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }

    /**
     * Verifies if an input string can be parsed as a double.
     *
     * @param s the string
     * @return {@code true} if the string can be parsed as a double, {@code false} otherwise
     */
    public static boolean isDouble(String s) {
        try {
            Double.parseDouble(s);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }

    /**
     * Makes a copy of a graph.
     *
     * @param inGraph the input graph
     * @return a copy of {@code inGraph}
     */
    public static GraphInterface copy(GraphInterface inGraph) {
        GraphInterface outGraph = inGraph.create();

        for (int node = 0; node < inGraph.getOrder(); node++)
            for (Edge e : inGraph.getOutEdges(node))
                if (!outGraph.hasEdge(e))
                    outGraph.addEdge(e);

        return outGraph;
    }

    /**
     * <p>Returns a graph with toggled edges.</p>
     *
     * <p>
     * If {@code inGraph} is directed, it returns an undirected graph with the same number of
     * nodes and an undirected edge for every directed edge of {@code inGraph}. If {@code inGraph} is
     * undirected, it returns a directed graph with directed edges \( (u,v) \) and \( (v,u) \) for
     * each undirected edge of {@code inGraph}.
     * </p>
     *
     * @param inGraph the input graph
     * @return the toggled graph
     */
    public static GraphInterface toggleDirected(GraphInterface inGraph) {
        GraphInterface outGraph;
        if (DirectedGraph.class.equals(inGraph.getClass()))
            outGraph = new UndirectedGraph(inGraph.getOrder());
        else if (UndirectedGraph.class.equals(inGraph.getClass()))
            outGraph = new DirectedGraph(inGraph.getOrder());
        else
            throw new IllegalArgumentException("Unknown class argument");

        for (int node = 0; node < inGraph.getOrder(); ++node)
            for (Edge e : inGraph.getOutEdges(node))
                if (!outGraph.hasEdge(e)) outGraph.addEdge(e);

        return outGraph;
    }

    /**
     * Reverses the input graph.
     *
     * @param graph the input graph
     * @return the reversed graph
     */
    public static DirectedGraph reverseGraph(DirectedGraph graph) {
        DirectedGraph rg = new DirectedGraph(graph.getOrder());

        for (int node = 0; node < graph.getOrder(); ++node)
            for (Edge e : graph.getOutEdges(node))
                rg.addEdge(e.getHead(), e.getTail(), e.getWeight());

        return rg;
    }
}
