package it.uniupo.graphlib;

/**
 * An interface to define all common method signatures for directed and undirected
 * graphs.
 *
 * @author Lavinia Egidi
 */
public interface GraphInterface {
    /**
     * Creates a new object of the same class as this, with the same number of nodes
     * and no edges.
     *
     * @return a graph with no edges, same number of nodes as this, same implementation
     */
    GraphInterface create();

    /**
     * Creates a new object of the same class as this, with order nodes and no edges.
     *
     * @param order the number of nodes
     * @return a graph with no edges, <i>order</i> nodes, same implementation
     */
    GraphInterface create(int order);

    /**
     * Adds to the graph a new edge.
     *
     * @param tail tail of the new edge
     * @param head head of the new edge
     */
    void addEdge(int tail, int head);

    /**
     * Adds to the graph a new edge.
     *
     * @param tail   tail of the new edge
     * @param head   head of the new edge
     * @param weight weight of the new edge
     */
    void addEdge(int tail, int head, int weight);

    /**
     * Adds to the graph a new edge.
     *
     * @param e the new edge that is to be added
     */
    void addEdge(Edge e);

    /**
     * Returns {@code true} if this graph has the specified edge, {@code false} otherwise.
     *
     * @param tail of the sought edge
     * @param head of the sought edge
     * @return {@code true} if the edge (tail,head) is in the graph
     */
    boolean hasEdge(int tail, int head);

    /**
     * Returns {@code true} if this graph has the specified edge, {@code false} otherwise.
     *
     * @param e the edge
     * @return {@code true} if the edge is in the graph
     */
    boolean hasEdge(Edge e);

    /**
     * The number of nodes of this graph.
     *
     * @return the number of nodes
     */
    int getOrder();

    /**
     * The number of edges of this graph.
     *
     * @return the number of edges
     */
    int getEdgeNum();

    /**
     * Removes the specified edge from the graph (if the edge exists).
     *
     * @param tail tail of the edge to be removed
     * @param head head of the edge to be removed
     */
    void removeEdge(int tail, int head);

    /**
     * Returns the neighbors of node {@code u}, as an iterable.
     *
     * @param u the node whose neighbors are required
     * @return an iterable containing the neighbors
     */
    Iterable<Integer> getNeighbors(int u);

    /**
     * Returns the edges incident to node {@code u}, as an iterable.
     *
     * @param u the node whose incident edges are required
     * @return an iterable containing the incident or outgoing edges
     */
    Iterable<Edge> getOutEdges(int u);

    /**
     * A string representation of the graph in the format: "\n" separated lines; first
     * line = number of nodes; each subsequent line is an edge in the format: head tail
     * weight.
     *
     * @return a string representing the graph
     */
    String serialize();
}
