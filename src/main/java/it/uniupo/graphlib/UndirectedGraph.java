package it.uniupo.graphlib;

import java.util.HashSet;
import java.util.LinkedList;

/**
 * A class to model a directed graph.
 *
 * @author Lavinia Egidi
 * @author Lorenzo Ferron
 */
public class UndirectedGraph extends AbstractGraph {

    /**
     * @param order the number of nodes
     * @see AbstractGraph#AbstractGraph(int)
     */
    public UndirectedGraph(int order) {
        super(order);
    }

    /**
     * @param graphDescription description of the graph
     * @see AbstractGraph#AbstractGraph(String)
     */
    public UndirectedGraph(String graphDescription) {
        super(graphDescription);
    }

    @Override
    public GraphInterface create(int order) {
        return new UndirectedGraph(order);
    }

    @Override
    public void addEdge(int tail, int head, int weight) {
        if (this.incidenceList[tail] == null)
            this.incidenceList[tail] = new LinkedList<>();
        if (this.incidenceList[head] == null)
            this.incidenceList[head] = new LinkedList<>();

        if (!hasEdge(tail, head)) {
            this.incidenceList[tail].add(new Edge(tail, head, weight));
            this.incidenceList[head].add(new Edge(head, tail, weight));
            ++this.edgeNum;
        }
    }

    @Override
    public void removeEdge(int tail, int head) {
        int edgeIndex = this.findEdge(tail, head);
        if (edgeIndex >= 0) {
            this.incidenceList[tail].remove(edgeIndex);
            edgeIndex = this.findEdge(head, tail);
            this.incidenceList[head].remove(edgeIndex);
            --this.edgeNum;
        }
    }

    @Override
    public String serialize() {
        StringBuilder buf = new StringBuilder();
        buf.append(this.getOrder()).append("\n");
        HashSet<Edge> hashSet = new HashSet<>();

        for (int node = 0; node < this.getOrder(); node++)
            for (Edge e : this.getOutEdges(node))
                if (!hashSet.contains(e) && !hashSet.contains(e.reverse())) {
                    hashSet.add(e);
                    buf.append(e.serialize());
                    buf.append("\n");
                }

        return buf.toString();
    }
}
