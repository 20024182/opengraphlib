package it.uniupo.graphlib;

import java.util.LinkedList;

/**
 * A class to model a directed graph.
 *
 * @author Lavinia Egidi
 * @author Lorenzo Ferron
 */
public class DirectedGraph extends AbstractGraph {

    /**
     * @param order the number of nodes
     * @see AbstractGraph#AbstractGraph(int)
     */
    public DirectedGraph(int order) {
        super(order);
    }

    /**
     * @param graphDescription description of the graph
     * @see AbstractGraph#AbstractGraph(String)
     */
    public DirectedGraph(String graphDescription) {
        super(graphDescription);
    }

    @Override
    public GraphInterface create(int order) {
        return new DirectedGraph(order);
    }

    @Override
    public void addEdge(int tail, int head, int weight) { // https://stackoverflow.com/a/268713
        if (this.incidenceList[tail] == null)
            this.incidenceList[tail] = new LinkedList<>();

        if (!this.hasEdge(tail, head)) {
            this.incidenceList[tail].add(new Edge(tail, head, weight));
            ++this.edgeNum;
        }
    }

    @Override
    public void removeEdge(int tail, int head) {
        int edgeIndex = this.findEdge(tail, head);
        if (edgeIndex >= 0) {
            this.incidenceList[tail].remove(edgeIndex);
            --this.edgeNum;
        }
    }

    @Override
    public String serialize() {
        StringBuilder buf = new StringBuilder();
        buf.append(this.getOrder()).append("\n");

        for (int node = 0; node < this.getOrder(); ++node)
            for (Edge e : this.getOutEdges(node)) {
                buf.append(e.getTail()).append(" ").append(e.getHead()).append(" ").append(e.getWeight());
                buf.append("\n");
            }

        return buf.toString();
    }
}
