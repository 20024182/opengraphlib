package it.uniupo.graphlib;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * A class to model an edge.
 *
 * @author Lavinia Egidi
 * @author Lorenzo Ferron
 */
public class Edge implements Comparable<Edge> {
    private int tail;
    private int head;
    private int weight;

    /**
     * Instances an edge with tail, head and weight. Call that if the edge is directed
     * tail and head are as follows (tail,head) if the edge is undirected, tail and
     * head are the two endpoints.
     *
     * @param tail   tail of this edge
     * @param head   head of this edge
     * @param weight weight of this edge
     */
    public Edge(int tail, int head, int weight) {
        this.tail = tail;
        this.head = head;
        this.weight = weight;
    }

    /**
     * Instances an edge with tail, head and weight 0. Call that if the edge is
     * directed tail and head are as follows (tail,head) if the edge is undirected,
     * tail and head are the two endpoints.
     *
     * @param tail tail of this edge
     * @param head head of this edge
     */
    public Edge(int tail, int head) {
        this(tail, head, 0);
    }

    /**
     * Instances a copy of an edge {@code e}.
     *
     * @param e a copy of this
     */
    public Edge(Edge e) {
        this(e.tail, e.head, e.weight);
    }

    /**
     * The tail of a directed edge \( (u,v) \) is \( v \). In an undirected edge, tail is one of
     * the edge's endpoints.
     *
     * @return tail of this edge
     */
    public int getTail() {
        return this.tail;
    }

    /**
     * Sets the value of the tail node.
     *
     * @param tail tail of this edge
     */
    public void setTail(int tail) {
        this.tail = tail;
    }

    /**
     * The head of a directed edge \( (u,v) \) is \( v \). In an undirected edge, head is one of
     * the edge's endpoints.
     *
     * @return head of this edge
     */
    public int getHead() {
        return this.head;
    }

    /**
     * Returns the weight of this edge. All object of the class always have a weight
     * that by default is set to 0.
     *
     * @return weight of this edge
     */
    public int getWeight() {
        return this.weight;
    }

    /**
     * @return The reverse of this edge (tail and head swapped)
     */
    public Edge reverse() {
        return new Edge(this.getHead(), this.getTail(), this.getWeight());
    }

    @Override
    public int hashCode() { // https://stackoverflow.com/a/27609
        return new HashCodeBuilder(17, 31). // two randomly chosen prime numbers
                append(this.head).
                append(this.tail).
                toHashCode();
    }

    @Override
    public boolean equals(Object obj) { // https://stackoverflow.com/a/27609
        // Basic checks.
        if (!(obj instanceof Edge))
            return false;
        if (obj == this)
            return true;

        // Property checks.
        Edge other = (Edge) obj;
        return new EqualsBuilder().
                append(this.head, other.head).
                append(this.tail, other.tail).
                isEquals();
    }

    public int compareTo(Edge o) {
        if (this.getTail() < o.getTail()) return -1;
        else if (this.getTail() == o.getTail() && this.getHead() < o.getHead()) return -1;
        else if (this.getTail() == o.getTail() && this.getHead() == o.getHead()) return 0;
        return 1;
    }

    /**
     * Returns this edge as a string (tail, head and weight in this order and separated
     * by blank).
     *
     * @return "tail head weight"
     */
    public String serialize() {
        return this.getTail() + " " + this.getHead() + " " + this.getWeight();
    }

    @Override
    public String toString() {
        return "(" + this.getTail() + "," + this.getHead() + ") w=" + this.getWeight();
    }
}
